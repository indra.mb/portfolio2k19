import { Injectable} from "@angular/core";

@Injectable()

export class worksData {
    works = [];

    constructor() {
        this.works = [
            {
                "projectName": "Actiwoo",
                "applicationame":"Social Fitness Platform",
                "tagLine": "Fitness at your interest",
                "category": "Fitness",
                "imageClass": "actiwoo",
                "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
                "details": {
                    "caption" : "A World BelowThe Surface",
                    "description": "Actiwoo is a social fitness platform who believes that “Everyone can be active”. It also encourages and motivates people to lead a physically active lifestyle. They give challenges to the users based on the simple activity they choose like running, walking, cycling etc., They have tie-ups with brands and other components to reward the user for all the hardwork they put in.",
                    "deliverables": "Trendy flat theme UX design, HTML5, CSS3, JQuery, Javascript, Pixel Perfect Development",
                    "screens": ["../../assets/images/projectImages/activoo/swiperImage-1.png", "../../assets/images/projectImages/activoo/swiperImage-2.png"],
                    "banner": "../../assets/images/banners/media&entertainment.png"
                }
            },
            {
                "projectName": "CCM Roofer",
                "applicationame":"Enterprise",
                "tagLine": "Best decision for your roof and home.",
                "category": "B2C",
                "imageClass": "ccmRoofer",
                "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
                 "details": {
                    "caption" : "A world below the surface",
                    "description": "CCM Roofer have been a full service roofing contractor for years in the local area. They possess many years of experience in the roofing industry. They work with quality materials and the manufacturers are trained to repair, install and maintain their materials. They stay abreast of the latest industry advances in order to offer their customers the best selection and options. They have built their reputation in the industry, one roof at a time.",
                    "deliverables": "Rich UI, Responsive and Pixel Perfect, Multi Platform Support, Unique Look and Feel",
                    "screens": ["../../assets/images/projectImages/CCMRoofer/swiperImage-1.png", "../../assets/images/projectImages/CCMRoofer/swiperImage-2.png"],
                    "banner": "../../assets/images/banners/manufacturing.png"
                }
            },
            {
                "projectName": "Fairway",
                "applicationame":"FinTech",
                "tagLine": "Mortgages made easy!",
                "category": "FinTech",
                "imageClass": "fairway",
                "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
                "details": {
                    "caption" : "A World Below The Surface",
                    "description": "Fairway Independent Mortgage Corp. wants to simplify the mortgage lending process in ways that will save customers time and money. It offers borrowers a robust suite of loan products and customer service touch points to do just that.",
                    "deliverables": "Pixel Perfect UI development, Multi Platform Support, Rich UI, Responsive and Pixel Perfect",
                    "screens": ["../../assets/images/projectImages/Fairway/swiperImage-1.png"],
                    "banner": "../../assets/images/banners/consumerlending.png"
                }
            },
            {
                "projectName": "HaiProperty",
                "applicationame":"Real Estate",
                "tagLine": "Helping find your place",
                "category": "Real Estate",
                "imageClass": "haiProperty",
                "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
                "details": {
                    "caption" : "A world below the surface",
                    "description": "Haiproperty is Kerala's Real Estate site that gives you complete information about Homes for Sale, Apartments for Rent, PG Rentals, Hostels, Plots for Sale, Commercial Building Space across Kerala. Haiproperty offers completely verified data, pictures of listed Properties, Maps for easy Search. Haiproperty provides Real Estate markets and trends to help you figure out exactly what, where and when to Buy, Sell, or Rent.",
                    "deliverables": "Visual Design, Browser Compatibility, Serverless Architecture",
                    "screens": ["../../assets/images/projectImages/Hai-Property/swiperImage-1.png"],
                    "banner": "../../assets/images/banners/manufacturing.png"
                }
            },
            {
              "projectName": "Experian",
              "applicationame":"Finance",
              "tagLine": "Beating Data into Emotions",
              "category": "Finance",
              "imageClass": "experian",
              "technologies": ["HTML5", "CSS3", "JQuery", "Javascript", "Angular"],
              "details": {
                  "caption" : "A world below the surface",
                  "description": "Experian is a leading global information services company, providing data and analytical tools to our clients around the world. In a complex, ever changing world, the use of data is now driving significant advancements and new ways of thinking.",
                  "deliverables": "Image Optimization, Rich in Widgets, Unique Look and Feel",
                  "screens": ["../../assets/images/projectImages/Experian/swiperImage-1.png", "../../assets/images/projectImages/Experian/swiperImage-2.png"],
                  "banner": "../../assets/images/banners/manufacturing.png"
              }
          },
          {
            "projectName": "Fuso",
            "applicationame":"Automobile",
            "tagLine": "Connecting the places",
            "category": "Automobile",
            "imageClass": "fuso",
            "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
            "details": {
                "caption" : "A world below the surface",
                "description": "The Mitsubishi Fuso Truck and Bus Corporation is a manufacturer of HYPERLINK trucks and HYPERLINK buses. It is headquartered in HYPERLINK Kawasaki, Kanagawa, Japan. Currently, it is 89.29 %-owned by Germany-based HYPERLINK Daimler AG, under the Daimler Trucks division. ",
                "deliverables": "Visual Design, Browser Compatibility, Serverless Architecture",
                "screens": ["../../assets/images/projectImages/Fuso/swiperImage-1.png", "../../assets/images/projectImages/Fuso/swiperImage-2.png", "../../assets/images/projectImages/Fuso/swiperImage-3.png"],
                "banner": "../../assets/images/banners/manufacturing.png"
            }
        },
        {
          "projectName": "Herc",
          "applicationame":"B2C",
          "tagLine": "Partnering in Transportations",
          "category": "B2C",
          "imageClass": "herc",
          "technologies": ["HTML5", "CSS3", "JQuery", "Javascript", "ReactJS"],
          "details": {
              "caption" : "A world below the surface",
              "description": "The Hertz Corporation, a subsidiary of Hertz Global Holdings Inc., is an American car rental company based in Estero, Florida that operates 9,700 international corporate and franchisee locations. As the second-largest US car rental company by sales, locations and fleet size, Hertz operates in 150 countries, including North America, Europe, Latin America, Africa, Asia, Australia, the Caribbean, the Middle East and New Zealand. The Hertz Corporation owns Dollar and Thrifty Automotive Group—which separates into Thrifty Car Rental and Dollar Rent A Car.",
              "deliverables": "Trendy flat theme UX design, HTML5, CSS3, JQuery, Javascript, Pixel Perfect Development",
              "screens": ["../../assets/images/projectImages/Herc/swiperImage-1.png", "../../assets/images/projectImages/Herc/swiperImage-2.png"],
              "banner": "../../assets/images/banners/manufacturing.png"
          }
      },
      {
        "projectName": "Hussman",
        "applicationame":"B2C",
        "tagLine": "Getting right to the good parts",
        "category": "B2C",
        "imageClass": "hussman",
        "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
        "details": {
            "caption" : "A world below the surface",
            "description": "Hussmann original parts that fit right and keep your equipment running like new.Hussmann experts in display cases and refrigeration systems and we provide parts that will deliver optimized performance for all Hussmann equipment.",
            "deliverables": "Rich UI, Responsive and Pixel Perfect, Multi Platform Support, Unique Look and Feel",
            "screens": ["../../assets/images/projectImages/Hussmann/swiperImage-1.png"],
            "banner": "../../assets/images/banners/ebusiness.png"
        }
    },
    {
      "projectName": "HYG",
      "applicationame":"Automobile",
      "tagLine": "Solutions driving Productivity",
      "category": "Automobile",
      "imageClass": "hyg",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "At Hyster-Yale Group, they not only build the best lift trucks possible, but to create solutions that enable the customers to improve the overall efficiency and effectiveness of their businesses. Product development is informed, on-going and proactive. Their wide global presence, among the largest of all material handling manufacturers, enables Hyster-Yale to commit the resources necessary to ensure the lift trucks are some of the most innovative in the industry.",
          "deliverables": "Pixel Perfect UI development, Multi Platform Support, Rich UI, Responsive and Pixel Perfect",
          "screens": ["../../assets/images/projectImages/HYG/swiperImage-1.png"],
          "banner": "../../assets/images/banners/capitalmarket.png"
      }
    },
    {
      "projectName": "Magento",
      "applicationame":"B2C",
      "tagLine": "Serving at doorstep",
      "category": "B2C",
      "imageClass": "magento",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "Magento is an ecommerce platform built on open source technology which provides online merchants with a flexible shopping cart system, as well as control over the look, content and functionality of their online store.",
          "deliverables": "Visual Design, Usability Analysis, UI testing",
          "screens": ["../../assets/images/projectImages/magento/swiperImage-1.png", "../../assets/images/projectImages/magento/swiperImage-2.png"],
          "banner": "../../assets/images/banners/ebusiness.png"
      }
    },
    {
      "projectName": "Nissan",
      "applicationame":"Automobile",
      "tagLine": "Shifting to way you move",
      "category": "Automobile",
      "imageClass": "nissan",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "Nissan Motor Company Ltd is a Japanese multinational automobile manufacturer headquartered in Nishi-ku, Yokohama. The company sells its cars under the Nissan, Infiniti and Datsun brands.",
          "deliverables": "Trendy flat theme UX design, HTML5, CSS3, JQuery, Javascript, Pixel Perfect Development",
          "screens": ["../../assets/images/projectImages/nissan/swiperImage-1.png"],
          "banner": "../../assets/images/banners/capitalmarket.png"
      }
    },
    {
      "projectName": "NewRez", 
      "applicationame":"FinTech",
      "tagLine": "Lending for Real Life",
      "category": "FinTech",
      "imageClass": "npf",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "NewRez LLC is a leading nationwide lender that serves diverse consumers with a customer-focused, responsive and reliable lending approach. Providing borrowers expertise and personal attention, the company offers some of the lowest mortgage.",
          "deliverables": "Visual Design, Browser Compatibility, Serverless Architecture",
          "screens": ["../../assets/images/projectImages/NPF/swiperImage-1.png"],
          "banner": "../../assets/images/banners/consumerlending.png"
      }
    },    
    {
      "projectName": "NS360", 
      "applicationame":"AgriTech",
      "tagLine": "Connecting Energy Markets",
      "category": "AgriTech",
      "imageClass": "ns360",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "Land O'Lakes, Inc. is a member-owned agricultural cooperative based in the Minneapolis-St. Paul suburb of Arden Hills, Minnesota, focusing on the dairy industry. The co-op has 1,959 direct producer-members, 751 member-cooperatives and about 10,000 employees who process and distribute products for about 300,000 agricultural producers; handling 12 billion pounds of milk annually. It is ranked third on the National Cooperative Bank Co-op 100 list of mutuals and cooperatives.",
          "deliverables": "Pixel Perfect UI development, Multi Platform Support, Rich UI, Responsive and Pixel Perfect",
            "screens": ["../../assets/images/projectImages/NS360/swiperImage-1.png"],
            "banner": "../../assets/images/banners/capitalmarket.png"
      }
    },  
    {
      "projectName": "WinField R7 Tool", 
      "applicationame":"AgriTech",
      "tagLine": "Connecting Energy Markets",
      "category": "AgriTech",
      "imageClass": "r7Tool",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "The R7® Tool is an industry-leading decision ag solution. It provides unbiased product performance information and critical field data to help optimize ROI potential. The R7® Tool uses satellite imagery and soil maps to reveal field variability.",
          "deliverables": "Pixel Perfect UI development, Multi Platform Support, Rich UI, Responsive and Pixel Perfect",
            "screens": ["../../assets/images/projectImages/winfield/R7-1.png", "../../assets/images/projectImages/winfield/R7-3.png", "../../assets/images/projectImages/winfield/R7-2.png"],
            "banner": "../../assets/images/banners/capitalmarket.png"
      }
    },  
    {
      "projectName": "SBI Cap",
      "applicationame":"Capital Markets",
      "tagLine": "Have a Better Home Investment",
      "category": "Capital Markets",
      "imageClass": "sbiCap",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "SBI Capital Markets Ltd. (SBICAP) is India’s largest domestic Investment Bank, offering the entire gamut of investment banking and corporate advisory services. These services encompass Project Advisory and Loan Syndication, Structured Debt Placement, Capital Markets, Mergers & Acquisitions, Private Equity and Stressed Assets Resolution.",
          "deliverables": "Component Rich Responsive Application, Responsive Widget Columns, Pixel Perfect Development, HighCharts",
          "screens": ["../../assets/images/projectImages/SBICAp/swiperImage-1.png", "../../assets/images/projectImages/SBICAp/swiperImage-2.png", "../../assets/images/projectImages/SBICAp/swiperImage-3.png", "../../assets/images/projectImages/SBICAp/swiperImage-4.png", "../../assets/images/projectImages/SBICAp/swiperImage-5.png"],
          "banner": "../../assets/images/banners/consumerlending.png"
      }
    },  
    {
      "projectName": "Hedge Serve",
      "applicationame":"Capital Markets",
      "tagLine": "More Value, Less Risk",
      "category": "Capital Markets",
      "imageClass": "hedgeServe",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "HedgeServ configures and integrates a risk management solution to meet each client’s unique reporting needs and frequencies, supported by a front office team of risk and valuation professionals.",
          "deliverables": "Pixel Perfect UI development, Multi Platform Support, Rich UI, Responsive and Pixel Perfect",
          "screens": ["../../assets/images/projectImages/HedgeServe/swiperImage-1.png"],
          "banner": "../../assets/images/banners/capitalmarket.png"
      }
    }, 
    {
      "projectName": "Radian",
      "applicationame":"FinTech",
      "tagLine": "Get the Dream home yearly",
      "category": "FinTech",
      "imageClass": "radian",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "Radian protects the American Dream for lenders and their borrowers by offering products and services that open doors for new generations of homeowners.Radian’s mortgage insurance (MI) products help borrowers become homeowners sooner by qualifying for loans with smaller downpayments.",
          "deliverables": "Visual Design, Usability Analysis, UI testing",
            "screens": ["../../assets/images/projectImages/Radian/swiperImage-1.png"],
            "banner": "../../assets/images/banners/consumerlending.png"
      }
    },
    {
      "projectName": "Verifone",
      "applicationame":"Capital Markets",
      "tagLine": "E-Payments made safer",
      "category": "Capital Markets",
      "imageClass": "verifone",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "Verifone, Inc. is an international producer and designer of electronic payment solutions.The company divides its business into two segments: Systems Solutions and Services. Systems Solutions consists of operations related to the sale of electronic payment products that enable electronic transactions.",
          "deliverables": "Component Rich Responsive Application, Responsive Widget Columns, Pixel Perfect Development, HighCharts",
        "screens": ["../../assets/images/projectImages/Verifone/swiperImage-1.png", "../../assets/images/projectImages/Verifone/swiperImage-2.png", "../../assets/images/projectImages/Verifone/swiperImage-2.png"],
        "banner": "../../assets/images/banners/capitalmarket.png"
      }
    },
    {
      "projectName": "IMGC",
      "applicationame":"Banking and Insurance",
      "tagLine": "A World Below The Surface",
      "category": "FinTech",
      "imageClass": "imgc",
      "technologies": ["HTML5", "CSS3", "JQuery", "Javascript"],
      "details": {
          "caption" : "A world below the surface",
          "description": "India’s first Mortgage Guarantee Company, India Mortgage Guarantee Corporation (IMGC) is a joint venture that combines the developmental mandate of National Housing Bank (NHB), the technical expertise of Genworth Financial and the resources of International Finance Corporation (IFC) & Asian Development Bank (ADB).",
          "deliverables": "Rich UI, Responsive and Pixel Perfect, Multi Platform Support, Unique Look and Feel",
            "screens": ["../../assets/images/projectImages/IMGC/swiperImage-1.png", "../../assets/images/projectImages/IMGC/swiperImage-2.png", "../../assets/images/projectImages/IMGC/swiperImage-3.png"],
            "banner": "../../assets/images/banners/consumerlending.png"
      }
    },
    ]
  }
}