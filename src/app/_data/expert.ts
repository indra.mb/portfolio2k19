import { Injectable } from "@angular/core";

@Injectable()

export class expertData {
    experts = [];
    constructor() {
            this.experts = [
                {
                    "_id":"4324324453464",
                    "firstName":"Pradeep Noble Raj",
                    "lastName":"",                    
                    "middleName":"",
                    "profileimage":"../../assets/images/Noble.png",
                    "profileimaghover":"../../assets/images/Noble-hover.png",
                    "disignation":"UX Architect",			
                    "skills":["Design Strategy","User Reaserch", "Rapid Prototyping", "Persona", "Information Architecture", "Card Sorting", "Task Flow", "User Journey Mapping", "Wire-framing", "Mockup Creation", "Usability Testing"],
                    "differentiator":["Explore new ideas and re-imagine digital experiences.","Resolve user’s problems through simple and intuitive user interfaces.","Expert in creating engaging visuals."],
                    "aboutMe":"I am a User Experience professional with 13 years of experience with a deep understanding of the User Centered Design process and human behavior. I have designed market-leading digital solutions on multiple channels.",
                    "certification":["Completed Usability Analyst from Human Factor International.","Certified Mortgage Associate for MBA.org."],
                    "bio":"All about User Experience",
                    "pdflink":"../../assets/pdf/Pradeep_Noble_Raj_Portfolio_For_UI.pdf"
                },
                {
                    "_id":"4324324453465",
                    "firstName":"Ravi",
                    "lastName":"Srimukunda",
                    "middleName":"",
                    "profileimage":"../../assets/images/Ravi.png",
                    "profileimaghover":"../../assets/images/Ravi-hover.png",
                    "disignation":"UX Architect",			
                    "skills":["Design Thinking","Personas", "User Flows/Scenarios","Heuristic Evaluation","User Interviews","Usability Testing","Competitor Analysis","Sketching","Wireframing","Mockups","Prototyping"],
                    "differentiator":["Observed and spoke to more than 500 users.","Structure and organize problems at both micro and macro levels.","Key contributor to a very popular pick-up and delivery service used across India.","Craft experiences that impact business metrics."],
                    "aboutMe":"I am a Product Designer, UX Researcher & Designer with over 16 years of experience in building consumer-centric products and enterprise products in varied domains such as E-commerce (B2C and C2C), FinTech (B2B & B2C), Manufacturing (B2B) and Agritech (B2C).",
                    "certification":["Insights for Innovation | IDEO U.","From Ideas to Action | IDEO U.","Innovation of Products and Services: MIT's Approach to Design Thinking."],
                    "bio":"Humanizing Technology",
                    "pdflink":""
                },
                {
                    "_id":"4324324453466",
                    "firstName":"Madhusudanan",
                    "lastName":"S",
                    "middleName":"",
                    "profileimage":"../../assets/images/Madhu.png",
                    "profileimaghover":"../../assets/images/Madhu-hover.png",
                    "disignation":"UX Architect",			
                    "skills":["Stakeholder Interviews","Personas", "User Flows/Scenarios","Design Principles","Heuristic Evaluation","User Interviews","Usability Testing","Competitor Analysis","Sketching/Wireframing","Interactive Prototypes"," Mockup Creation"],
                    "differentiator":["Enquire relentlessly to identify the core problems in the journey.","Creating new user flows with a deep understanding of the requirements and needs."],
                    "aboutMe":"I am a User Experience (UX) Architect with 15+ years of experience in the area of Digital Experience, Innovation and Customer Experience. My core competencies include Design Thinking and User Centered Design. I focus on understanding user needs, analyzing requirements, creating journey maps, defining user flows, prototyping using sketches or wireframes and producing high-fidelity mockups. I am passionate about identifying, defining and solving the right problem to fulfill the user’s and business needs.",
                    "certification":["Certified Usability Analyst from Human Factor International."],
                    "bio":"Make UX simple, but significant.",
                    "pdflink":"../../assets/pdf/Madhusudhanan_Portfolio.pdf"
                },
                {
                    "_id":"4324324453470",
                    "firstName":"Elena Page",
                    "lastName":"",
                    "middleName":"",
                    "profileimage":"../../assets/images/elena.png",
                    "profileimaghover":"../../assets/images/elena.png",
                    "disignation":"UX Lead",			
                    "skills":["Design Strategy","User Reaserch","Rapid Prototyping","Persona","Information Architecture","Card Sorting","Task Flow","User Journey Mapping","Wire-framing","Mockup Creation","Usability Testing"],
                    "differentiator":["Experience Designer | Researcher | Strategist"],
                    "aboutMe":"I am a multidisciplinary designer, art director and instructor living and working in Raleigh, NC. I am a designer with hands-on experience in the fields of motion graphics, print, experience design, e-learning and education.",
                    "certification": [],
                    "bio":"Story-driven experiences together!"
                },   
                {
                    "_id":"4324324453467",
                    "firstName":"Basuvalingappa",
                    "lastName":"A",
                    "middleName":"",
                    "profileimage":"../../assets/images/Basu.png",
                    "profileimaghover":"../../assets/images/Basu-hover.png",
                    "disignation":"Associate UX Architect",			
                    "skills":["Branding","User Research","Usability Evaluation","Persona","User Journey Mapping","Information Architecture","Card Sorting","Task Flow","Wire-framing","Rapid Prototyping","Creative Visual Design","Interaction Design","Design Management"],
                    "differentiator":["Help brands by imagining and designing new experiences.","Passionate about creating experiences that are game-changing leading to new user behavior.","Proven abilities in building concepts, planning, design and execution."],
                    "aboutMe":"I am a User Experience professional with 11+ years of industry experience in Customer Experience, Digital Strategy and Creative Visual Design for different channels such as web, tablet, mobile and wearable devices.",
                    "certification":["Certified Usability Analyst (Human Factor International - CUA).","Certified Mortgage Associate from MBA.org.","Advanced User Experience Researcher by LinkedIn."],
                    "bio":"Think. Human. Experience.",
                    "pdflink":"../../assets/pdf/Basuvalingappa_A_N0077_Nov2019_Portfolio.pdf"
                },
                {
                    "_id":"4324324453468",
                    "firstName":"Shemin Krishnan",
                    "lastName":"K ",
                    "middleName":"",
                    "profileimage":"../../assets/images/shemin.png",
                    "profileimaghover":"../../assets/images/shemin-hover.png",
                    "disignation":"Associate UX Architect",			
                    "skills":["UX/UI Design","Wireframing","Low to high fidelity prototyping ","Graphic design","Visual design","Style guides","User flows","User journey mapping","Video Production","Motion design","Material design","Product Visualization"],
                    "differentiator":["Over 10 years in design I’m still exploring different avenues to express my creativity. "],
                    "aboutMe":"Hello, I’m a digital designer & Motion Graphic Artist. As a designer, I’m listening to people and make complicated things understandable. While designing, I try to keep things simple. I believe in lifelong learning, a good feedback culture and an open mind.",
                    "certification":["Diploma in Designing and Animation ", "Mobile User Experience - Interaction Design Foundation"],
                    "bio":"Simple is hard. Easy is harder. Invisible is hardest",
                    "pdflink":""
                },                
                {
                    "_id":"4324324453469",
                    "firstName":"Gilbert Dawson",
                    "lastName":"",
                    "middleName":"",
                    "profileimage":"../../assets/images/Gilbert.png",
                    "profileimaghover":"../../assets/images/Gilbert-hover.png",
                    "disignation":"Associate UX Architect",			
                    "skills":["Design Strategy","User Reaserch","Rapid Prototyping","Persona","Information Architecture","Card Sorting","Task Flow","User Journey Mapping","Wire-framing","Mockup Creation","Usability Testing"],
                    "differentiator":["13 years of experience and a proven ability to resolve user's problems through simple and beautiful user interfaces."],
                    "aboutMe":"I am a User Experience proefessional with 13 years of experience and a proven ability to resolve user's problems through simple and beautiful interfaces.  With a deep understanding of the UCD process and human behavior.  I have designed market-leading designs for both mobile and desktop.",
                    "certification": ["Completed Usability Analyst from Human Factor International", "Certified Mortgage Associate for MBA.org"],
                    "bio":"Breath UX",
                    "pdflink":"../../assets/pdf/Gilbert_Dawson_DesignPortfolio.pdf"
                }
                                           
            ]
        }
}