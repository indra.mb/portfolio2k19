import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent } from './app.component';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { appRoutingModule } from "./app-routing.module";
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material';
import { UxprocessComponent } from './uxprocess/uxprocess.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { ExpertsComponent } from './experts/experts.component';
import { ExpertDescriptionComponent } from './expert-description/expert-description.component';
import { OurWorksComponent } from './our-works/our-works.component';
import { OurWorksDetailComponent } from './our-works-detail/our-works-detail.component';
import { worksData } from './_data/worksData'
import { expertData } from "src/app/_data/expert";
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgScrolltopModule } from 'ng-scrolltop';
@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        appRoutingModule,
        MatInputModule,
        MatFormFieldModule,
        SlickCarouselModule,
        MatDialogModule,
        NgScrolltopModule,
        ScrollToModule.forRoot()
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        UxprocessComponent,
        ExpertsComponent,
        ExpertDescriptionComponent,
        OurWorksComponent,
        OurWorksDetailComponent
    ],
    exports: [
      MatFormFieldModule,
      MatInputModule,
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        // provider used to create fake backend
        fakeBackendProvider,
        worksData,
        expertData
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }