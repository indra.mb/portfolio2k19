﻿import { Component, AfterViewInit, ViewChild, ViewEncapsulation, HostListener, ElementRef, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import * as $ from 'jquery';
import { WOW } from 'wowjs/dist/wow.min';
import { User } from '../_models';
import { UserService, AuthenticationService } from '../_services';

@Component({ 
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterViewInit{
    loading = false;
    currentUser: User;
    userFromApi: User;
    public show: boolean = true;
    public disabled: boolean = false;
    ENTER_KEYCODE = 13;
    activeSection = 0;
    hoverflag:boolean;
    hoverclass: boolean;
    @ViewChild('swiperslide1', {static: true}) swiperslide1: ElementRef;
    @ViewChild('swiperslide2', {static: true}) swiperslide2: ElementRef;
    @ViewChild('swiperslide3', {static: true}) swiperslide3: ElementRef;
    @ViewChild('swiperslide4', {static: true}) swiperslide4: ElementRef;
    @ViewChild('swiperslide5', {static: true}) swiperslide5: ElementRef;
    @ViewChild('swiperslide6', {static: true}) swiperslide6: ElementRef;
    public currentActive = 0;
    public slider1: any = null;
    public slider2: any = null;
    public slider3: any = null;
    public slider4: any = null;
    public slider5: any = null;
      constructor( private userService: UserService, private authenticationService: AuthenticationService ) {
        this.currentUser = this.authenticationService.currentUserValue;
        this.hoverclass = false;
    }
    slideConfig = {
        "slidesToShow": 8, 
        "slidesToScroll": 1,
        "speed": 300,
        "dots":false,
        "infinite": true,
        "autoplay": true,
        "arrows": false
    };
    ngOnInit() {
        this.loading = true;
        this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
            this.loading = false;
            this.userFromApi = user;
        });
        $(document).unbind('keypress');       
    }
    
    ngAfterViewInit() {        
        this.slider1 = this.swiperslide1.nativeElement.offsetTop;
        this.slider2 = this.swiperslide2.nativeElement.offsetTop;
        this.slider3 = this.swiperslide3.nativeElement.offsetTop;
        this.slider4 = this.swiperslide4.nativeElement.offsetTop;
        this.slider5 = this.swiperslide5.nativeElement.offsetTop;      
        new WOW().init();
        $(window).scrollTop($(window).scrollTop()+1);
        $(document).keypress( (e) => {
            if (e.which == 13 || e.which == 32 || e.which == 40 ) {
                e.preventDefault();                 
                const el = $('.swiper-pagination li a.active').parent().next().find('a').attr('ng-reflect-ngx-scroll-to');
                document.querySelector(el).scrollIntoView({ behavior: 'smooth', block: 'center' });                              
            }
        });
        $(document).keydown( (e) => {
            if(e.which == 40) { 
                const el = $('.swiper-pagination li a.active').parent().next().find('a').attr('ng-reflect-ngx-scroll-to');
                document.querySelector(el).scrollIntoView({ behavior: 'smooth', block: 'center' });
                return false;
            }
            else if(e.which == 38){
                const el = $('.swiper-pagination li a.active').parent().prev().find('a').attr('ng-reflect-ngx-scroll-to');
                document.querySelector(el).scrollIntoView({ behavior: 'smooth', block: 'center' });
                return false;
            }
        });
    }
    @HostListener('window:scroll', ['$event'])
    checkOffsetTop() {        
        this.activeSection = 0;
        if (window.pageYOffset >= this.slider1 && window.pageYOffset < this.slider2) {
            this.currentActive = 1;
            $('#ziwrap_left').hide();
        } else if (window.pageYOffset >= this.slider2 && window.pageYOffset < this.slider3) {
            this.currentActive = 2;
            $('#ziwrap_left').show();
            document.getElementById("navbar").style.top = "0";
            $('.count').prop('Counter',0).animate({
                Counter: 75
            }, {
                duration: 1500,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        } else if (window.pageYOffset >= this.slider3 && window.pageYOffset < this.slider4) {
            this.currentActive = 3;       
            document.getElementById("navbar").style.top = "0";     
        } else if (window.pageYOffset >= this.slider4 && window.pageYOffset < this.slider5) {
            this.currentActive = 4;
            document.getElementById("navbar").style.top = "0";
        }else if (window.pageYOffset >= this.slider5) {
            this.currentActive = 5;
            document.getElementById("navbar").style.top = "0";
        }
    }    
}