import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { UxprocessComponent } from './uxprocess/uxprocess.component';
import { AuthGuard } from './_helpers';
import { ExpertsComponent } from "src/app/experts/experts.component";
import { ExpertDescriptionComponent } from "src/app/expert-description/expert-description.component";
import { OurWorksComponent } from './our-works/our-works.component';
import { OurWorksDetailComponent } from './our-works-detail/our-works-detail.component';
const routes: Routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'uxprocess',
        component: UxprocessComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'experts',
        component: ExpertsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'profile/:id',
        component: ExpertDescriptionComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'works',
        component: OurWorksComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'works-desc/:projectName',
        component: OurWorksDetailComponent,
        canActivate: [AuthGuard]
    },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);