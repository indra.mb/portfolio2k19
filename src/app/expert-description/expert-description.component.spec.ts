import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertDescriptionComponent } from './expert-description.component';

describe('ExpertDescriptionComponent', () => {
  let component: ExpertDescriptionComponent;
  let fixture: ComponentFixture<ExpertDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
