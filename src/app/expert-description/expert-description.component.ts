import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { expertData } from "src/app/_data/expert";
@Component({
  selector: 'app-expert-description',
  templateUrl: './expert-description.component.html',
  styleUrls: ['./expert-description.component.less']
})
export class ExpertDescriptionComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;
  expertsData = [];
  expert = [];
  constructor(private router: Router, private route: ActivatedRoute, private _data: expertData) {
    this.expertsData = this._data.experts
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });
    function findId(expertsData, id) {
      for (var i = 0; i < expertsData.length; i++) {
        if (expertsData[i]._id == id) {
          return (expertsData[i]);
        }
      }
    }
    //debugger
    this.expert = findId(this.expertsData, this.id);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
