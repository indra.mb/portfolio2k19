import { Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { worksData } from '../_data/worksData';
import * as $ from 'jquery';

@Component({
  selector: 'app-our-works',
  templateUrl: './our-works.component.html',
  styleUrls: ['./our-works.component.less']
})
export class OurWorksComponent implements OnInit {
  worksData = [];
  categories = [];
  tagLines = [];
  filterBy = "";
  filterData = [];
  filters = [];
  technologiesList = ["HTML", 'CSS3', 'Javascript', 'JQuery', 'Angular', 'ReactJS'];
  constructor(private router: Router, private _data: worksData) {
    this.worksData = this._data.works;
    this.categories = this.getUniequeValues('category');
    this.tagLines = this.getUniequeValues('tagLine');
   }



  ngOnInit() {
    window.scroll(0,0);
    $('body').click( (event) => {
      if(event.target.className.length && event.target.className.lastIndexOf('modal-backdrop') >= 0) {
          $('.close')[0].click();
          this.filterBy = "";
      }
    })
    
  }

  changeFilter(data, category) {
    var pos = window.scrollY + document.querySelector('#test').getBoundingClientRect().top;
    document.querySelector('#exampleModalLong')['style'].top = pos+52+'px';
    this.filters = data;
    this.filterBy = category;
  }

  resetFilter() {
    this.worksData = this._data.works;
    $(".option-item input[type=checkbox]").each(function () {
          $(this).prop("checked", false);
      });
    this.filterData = [];
  }

  filterWorks(index, val) {
    this.worksData = this._data.works;
    let filteredData = [];
    
    if (this.filterData.length <= 0) {
      this.filterData.push(this.filters[index]);
    } else if(!this.filterData.includes(val) ){
      this.filterData.push(val)
    } else {
      this.filterData.splice(this.filterData.lastIndexOf(val), 1);
    }

    this.worksData.forEach((element, index) => {
      this.filterData.forEach((e, ind) => {
        if (this.filterBy !== 'technologies') {
          if (element[this.filterBy] === e) {
            filteredData.push(element);
          }
        } else {
          if (JSON.stringify(element[this.filterBy]).lastIndexOf(e) >= 0) {
            filteredData.push(element);
          }
        }
      });
    });

    if (filteredData.length > 0) {
      filteredData = filteredData.filter(function (elem, index, self) {
        return index === self.indexOf(elem);
      })
      this.worksData = filteredData;
    }
  }

  
  getUniequeValues(key) {
    let arr = [];
    this.worksData.forEach( element => {
      if(!arr.includes(element[key])) {
        arr.push(element[key]);
      }
    });
    
    return arr;
  }

  goToProductDetails(projectName) {
    this.router.navigate(['/works-desc', projectName]);
  }
}
