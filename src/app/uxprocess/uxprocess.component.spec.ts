import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UxprocessComponent } from './uxprocess.component';

describe('UxprocessComponent', () => {
  let component: UxprocessComponent;
  let fixture: ComponentFixture<UxprocessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UxprocessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UxprocessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
