import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, OnDestroy, ElementRef, HostListener } from '@angular/core';
import { worksData } from '../_data/worksData';
import * as $ from 'jquery';
import { WOW } from 'wowjs/dist/wow.min';

@Component({
  selector: 'client-description',
  templateUrl: './our-works-detail.component.html',
  styleUrls: ['./our-works-detail.component.less']
})
export class OurWorksDetailComponent implements OnInit {
  projectName: string;
  private sub: any;
  worksData = [];
  project = [];
  showArrow = false;
  slideConfig = {
        "slidesToShow": 1, 
        "speed": 300,
        "dots":false,
        "infinite": true,
        "autoplay": false,
        "arrows": true
    };
  constructor(private router: Router, private route: ActivatedRoute,
   private _data: worksData, private myElement: ElementRef) {
    this.worksData = this._data.works;
   }

  ngOnInit() {
    window.scroll(0,0);
    this.sub = this.route.params.subscribe(params => {
      this.projectName = params['projectName'];
    });
    function getProjectDetails(worksData, projectName) {
      for (var i = 0; i < worksData.length; i++) {
        if (worksData[i].imageClass == projectName) {
          return (worksData[i]);
        }
      }
    }
    this.project = getProjectDetails(this.worksData, this.projectName);
  }

  scrollTo() {
    let el = this.myElement.nativeElement.querySelector('#prjectScreenShots');
    el.scrollIntoView({ behavior: 'smooth', block: 'center' });
  }
  ngAfterViewInit() {
    this.myElement.nativeElement.querySelector('.slick-next.slick-arrow')
                                  .addEventListener('click', this.onClick.bind(this));
    this.myElement.nativeElement.querySelector('.slick-prev.slick-arrow')
                                  .addEventListener('click', this.onClick.bind(this));
  }
  onClick(event) {
    var currentElementTarget = event.currentTarget
   if(currentElementTarget.classList.contains("active")) {
    currentElementTarget.classList.remove("active");
   }
   else {
    currentElementTarget.classList.add("active");
   }

   setTimeout(function () {
     if($('.slick-next.slick-arrow')[0].classList.contains("active")) {
      $('.slick-next.slick-arrow')[0].classList.remove("active")
     }
     if($('.slick-prev.slick-arrow')[0].classList.contains("active")) {
      $('.slick-prev.slick-arrow')[0].classList.remove("active")
     }
    
  }, 500)
  }

  @HostListener('window:scroll', ['$event'])
    checkOffsetTop() { 
      this.showArrow = false;       
        if (window.pageYOffset >= 100) {
          this.showArrow = true;
        } 
    }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  scrollTotop(){
    window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
  }
}