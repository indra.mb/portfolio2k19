import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { expertData } from "src/app/_data/expert";

@Component({
  selector: 'app-experts',
  templateUrl: './experts.component.html',
  styleUrls: ['./experts.component.less']
})
export class ExpertsComponent implements OnInit {
  expertsData = [];
  constructor(private router: Router, private _data: expertData) { 
    this.expertsData = this._data.experts;
  }

  ngOnInit() {
  }

  goToProductDetails(id) {
    this.router.navigate(['/profile', id]);
  }

}
