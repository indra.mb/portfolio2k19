import { Component, HostListener } from '@angular/core';
import { Router, Event } from '@angular/router';

import { AuthenticationService } from './_services';
import { User, Role } from './_models';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {
    title = 'UI-Portfolio';
    currentUser: User;
    showHeader = true;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

        this.router.events.subscribe((event: Event) => {
            if (event && event['url']) {
                if (event['url'].lastIndexOf('works-desc') >= 0) { 
                    this.showHeader = false;
                } else {
                    this.showHeader = true;
                }
            }
        })
    }

    get isAdmin() {
        return this.currentUser && this.currentUser.role === Role.Admin;
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }

   /* @HostListener('window:scroll', ['$event'])
    onWindowScroll(e) {
        if (window.pageYOffset > 100) {
        let element = document.getElementById('navbar');
            document.getElementById("navbar").style.top = "-100px";
        } else {
        let element = document.getElementById('navbar');
            document.getElementById("navbar").style.top = "0";
        }
    }*/

}